#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>

#define SIZE_M 20000
#define SIZE_N 20000
#define NUM_THREADS 16

int A[SIZE_M][SIZE_N];

int main() {
    int sum = 0;
    double avg = 0;

	srand(78345);
	for (int i = 0; i < SIZE_M; ++i) {
		for (int j = 0; j < SIZE_N; ++j) {
			A[i][j] = rand();
		}
	}

    for (int m = 0; m < SIZE_M; m++){
        for (int n = 0; n < SIZE_N; n++){
            sum += A[m][n];
        }
    }

    avg = (double)sum / (SIZE_M * SIZE_N);
    printf("Average Value=%.2f\n", avg);
}

