#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

#define SIZE_M 20000
#define SIZE_N 20000
#define NUM_THREADS 16

int A[SIZE_M][SIZE_N];

typedef struct {
	int start;
	int end;
	int sum;
} thread_param;

void* line_sum(void* arg);

int main() {
    int sum = 0;
    double avg = 0;

	// Init
	srand(78345);
	for (int i = 0; i < SIZE_M; ++i) {
		for (int j = 0; j < SIZE_N; ++j) {
			A[i][j] = rand();
		}
	}

	int step = SIZE_M * SIZE_N / NUM_THREADS;
	pthread_t threads[NUM_THREADS];
	thread_param params[NUM_THREADS];
	for (int i = 0; i < NUM_THREADS; ++i) {
		params[i].start = i * step;
		params[i].end = (i + 1) * step;
		pthread_create(&threads[i], NULL, line_sum, &params[i]);
	}

    for (int m = 0; m < SIZE_M; m++) {
        for (int n = 0; n < SIZE_N; n++) {
            sum += A[m][n];
        }
    }

    avg = (double)sum / (SIZE_M * SIZE_N);
    printf("Average Value=%.2f\n", avg);
}

void* line_sum(void* arg) {
	thread_param* param = (thread_param*) arg;
	int start_m = param->start / SIZE_N;
	int start_n = param->start % SIZE_N;
	int end_m = param->end / SIZE_N;
	int end_n = param->end % SIZE_N;

	// Use stack local variable to avoid false sharing
	int sum = 0;
	for (int m = start_m; m < end_m; ++m) {
		for (int n = start_n; n < end_n; ++n) {
			sum += A[m][n];
		}
	}
	param->sum = sum;

	return NULL;
}

