#include <cstdint>
#include <format>
#include <iostream>
#include <vector>

using namespace std;

uint64_t horner(const vector<uint64_t>& coeffs, uint64_t x) {
	uint64_t result = coeffs[0];
	for (size_t i = 1; i < coeffs.size(); ++i) {
		result = result * x + coeffs[i];
	}
	return result;
}

int main() {
	cout << format("Num is ") << horner({2, 3}, 2) << endl;
	cout << format("Expected: {}\n", 2 * 2 + 3);
}

