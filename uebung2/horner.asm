SECTION .text

EXTERN printf
GLOBAL main

main:
push rbp
mov rbp, rsp

mov rbx, koeffizient
mov rax, qword [x]
imul qword [rbx+0]
add rax, qword [rbx+8]
imul rax, qword [x]
add rax, qword [rbx+16]
mov qword [result], rax

; print result
mov rdi, fmt
mov rsi, [result]
mov rax, 0
call printf wrt ..plt
pop rbp
mov rax, 0
ret

SECTION .data
fmt db "%d", 10, 0
x dq 3
koeffizient dq 33, 2, 7
result dq 0
