section .text
global main

main:
push rbp
mov rbp, rsp
mov rax, 1
mov rcx, [n]

fac:
imul rax, rcx
loop fac

mov [ergebnis], rax

pop rbp
mov rax, 0
ret

section .data
n dq 4
ergebnis dq 0
