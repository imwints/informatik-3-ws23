DEFAULT REL
SECTION .data
n dq 4
 ; Variable n
ergebnis dq 0 ; Variable fuer Ergebnis
output_str db " Fakultaet von n = %u", 10, " ergebnis : %u", 10, 0
SECTION .text
global main
extern printf
main:
mov rcx , qword [n] ; zu berechnende Zahl
mov rax , 1
 ; 1 ist Ergebnis fuer n=0
cmp rcx , 0
je ende
 ; wenn nichts zu berechnen
l1:
mul rcx ; rdx:rax = rax * rcx
loop l1 ; verringert rcx , prueft dann auf 0
ALIGN 8
ende:
mov qword [ ergebnis ], rax ; Ergebnis sichern
; Ausgabe ( optional !)
sub rsp , 8
mov rdi , qword output_str
mov rsi , qword [n]
mov rdx , qword [ ergebnis ]
call printf
add rsp , 8
mov rax , 0
ret
