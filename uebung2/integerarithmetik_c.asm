SECTION .text

EXTERN printf
GLOBAL main

main:
push rbp
mov rbp, rsp

mov rbx, koeffizient
mov rax, qword [x]
imul rax	; rax = rax * rax
imul rax, qword [rbx+0]	; rbx = rax * koeffizient[0]

mov rcx, [x] 	; rcx = x
imul rcx, qword [rbx+8]	; rcx = rcx * koeffizient[1]

add rax, rcx	; rax = rax + rcx
add rax, qword [rbx+16]	; rax = rax + koeffizient[2]
mov [result], rax

; print result
mov rdi, fmt
mov rsi, [result]
mov rax, 0
call printf wrt ..plt
pop rbp
mov rax, 0
ret

SECTION .data
fmt db "%d", 10, 0
x dq 3
koeffizient dq 33, 2, 7
result dq 0
