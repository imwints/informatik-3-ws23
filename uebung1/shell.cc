#include <filesystem>
#include <format>
#include <iostream>
#include <ranges>
#include <string_view>
#include <vector>

#include <sys/wait.h>
#include <unistd.h>

int main(int argc, char* argv[]) {
	if (argc > 1) {
		std::vector<std::string_view> args { argv + 1, argv + argc };

		auto pid = vfork();
		if (pid < 0) {
			// Forking failed
			return 1;
		}

		if (pid == 0) {
			execvp(argv[1], argv + 1);
		} else {
			int status {};
			waitpid(pid, &status, 0);
			if (WIFEXITED(status)) {
				return WEXITSTATUS(status);
			}
			else if (WIFSIGNALED(status)) {
				return WTERMSIG(status);
			}
			else {
				return 1;
			}
		}
	}

	std::string line;
	bool exit = false;
	while (!exit) {
		std::cout << std::format("{} # ", std::filesystem::current_path().string());
		std::getline(std::cin, line);
		if (line == "exit") {
			break;
		}

		auto program = line.substr(0, line.find(' '));
		auto program_cstr = program.c_str();
		auto args = line.substr(line.find(' ')) | std::views::split(' ');
		std::vector<char*> argv;
		for (auto arg : args) {
			argv.push_back(arg.data());
		}
		auto argv_ptr = argv.data();

		auto pid = vfork();
		if (pid < 0) {
			// Forking failed
			return 1;
		}

		if (pid == 0) {
			execvp(program_cstr, argv_ptr);
			_exit(0);
		} else {
			int status {};
			waitpid(pid, &status, 0);
		}
	}
};
