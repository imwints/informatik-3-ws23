#include <cstring>
#include <iostream>
#include <format>
#include <sys/wait.h>
#include <unistd.h>

int main(int argc, char* argv[]) {
	if (argc < 2) {
		return 0;
	}

	auto pid = fork();

	// Child process
	if (pid == 0) {
		execvp(argv[1], argv + 1);
	} else {
		std::cout << std::format("Child process pid: {}\n", pid);
		int status;
		wait(&status);
		std::cout << std::format("Child process exited with status: {}\n", status);
	}
}
