#include <cstdlib>
#include <cstring>
#include <format>
#include <iostream>
#include <string_view>

int main(int argc, char* argv[]) {
	if (argc < 2) {
		return 0;
	}

	std::string_view env {};
	bool verbose = false;
	for (int i = 1; i < argc; ++i) {
		using std::string_view_literals::operator""sv;
		if (argv[i] == "-v"sv) {
			verbose = true;
			if (!env.empty()) {
				break;
			}
		} else {
			env = argv[i];
			if (verbose) {
				break;
			}
		}
	}

	std::string_view value {};
	{
		const char* env_char = std::getenv(env.data());
		if (env_char != nullptr) {
			value = env_char;
		}
	}
	if (value != "") {
		if (verbose) {
			std::cout << std::format("{} = {}\n", env, value);
		}
		return 0;
	} else {
		return 1;
	}
}
